<?php
/**
 *
 * The template for displaying all pages.
 * @since 1.0.0
 * @version 1.0.1
 *
 */
get_header();
get_template_part( 'templates/page-header' );

$cs_post_meta    = get_post_meta( $post->ID, '_custom_page_options', true );
$cs_page_layout  = ( isset ( $cs_post_meta['sidebar'] ) ) ? $cs_post_meta['sidebar'] : 'full';
$cs_page_column  = ( $cs_page_layout == 'full' ) ? '12' : '9';
$vc_use_default  = cs_get_option( 'vc_use_default' );
$cs_page_padding = ( $vc_use_default !== true ) ? 'md-padding' : '';

if( ( $cs_page_layout == 'fluid' || isset( $cs_post_meta['section'] ) ) && $vc_use_default !== true ) {

  get_template_part('templates/page', 'section');

} else {
?>
<section class="main-content <?php echo $cs_page_padding; ?> page-layout-<?php echo $cs_page_layout; ?>">
  <div class="container">
    <div class="row">

      <?php cs_page_sidebar( 'left', $cs_page_layout ); ?>
    
      <div class="col-md-<?php echo $cs_page_column; ?>">
        <div class="page-content">
          <?php
            // Start the Loop.
            while ( have_posts() ) : the_post();

              cs_page_featured_image();
              the_content();
              cs_link_pages();
              do_action( 'cs_page_end' );

            endwhile;
          ?>
        </div>
      </div>

      <?php cs_page_sidebar( 'right', $cs_page_layout ); ?>

    </div>
  </div>
</section>
<?php
}
get_footer();