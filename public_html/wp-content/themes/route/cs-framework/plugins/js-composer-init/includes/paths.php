<?php
/**
 * Get file/directory path in Vc.
 *
 * @param string $name - path name
 * @param string $file
 * @return string
 */
function cs_vc_path_filter( $path ){

  if( basename( $path ) == 'map.php' ) {
    return FRAMEWORK_PLUGIN_DIR . '/js-composer-init/config/map.php';
  }

  return $path;

}
add_filter( 'vc_path_filter', 'cs_vc_path_filter' );