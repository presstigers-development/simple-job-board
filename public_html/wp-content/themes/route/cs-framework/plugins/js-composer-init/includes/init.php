<?php
/**
 * Main Visual composer manager.
 * @var Vc_Manager - instance of composer management.
 */
global $vc_manager;
$vc_manager->setIsAsTheme();
$vc_manager->disableUpdater();
$vc_manager->setEditorDefaultPostTypes( array( 'page', 'post', 'portfolio' ) );

// front end option
if( cs_get_option( 'vc_front_end' ) !== true ) {
  $vc_manager->frontendEditor()->disableInline();
}