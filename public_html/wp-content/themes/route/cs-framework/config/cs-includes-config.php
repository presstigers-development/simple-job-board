<?php
/**
 *
 * Load all of shortcode from folder
 * @since 1.0.0
 * @version 1.1.0
 *
 */
//
// Require plugin.php to use is_plugin_active() below
// ----------------------------------------------------------------------------------------------------
if ( ! function_exists( 'is_plugin_active' ) ) {
  include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
}

//
// Load Shortcodes
// ----------------------------------------------------------------------------------------------------
foreach ( glob( FRAMEWORK_INCLUDE_DIR . '/shortcodes/cs_*.php' ) as $shortcode ) {
  locate_template( 'cs-framework/includes/shortcodes/'. basename( $shortcode ), true );
}

//
// Custom Style Adapted
// ----------------------------------------------------------------------------------------------------
locate_template( 'cs-framework/includes/custom-style.php', true );

//
// woocommerce integration
// ----------------------------------------------------------------------------------------------------
if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
  locate_template( 'cs-framework/plugins/woocommerce/woocommerce-config.php', true );
}

//
// TGM integration
// ----------------------------------------------------------------------------------------------------
locate_template( 'cs-framework/plugins/tgm-plugin-activation/tgm-route-plugins.php', true );

//
// Visual Composer integration
// ----------------------------------------------------------------------------------------------------
$vc_use_default = cs_get_option( 'vc_use_default' );
if ( is_vc_activated() && $vc_use_default !== true ) {
  locate_template( 'cs-framework/plugins/js-composer-init/includes/paths.php', true );
  locate_template( 'cs-framework/plugins/js-composer-init/includes/init.php', true );

  foreach ( glob( FRAMEWORK_INCLUDE_DIR . '/shortcodes/vc_*.php' ) as $shortcode) {
    locate_template( 'cs-framework/includes/shortcodes/'. basename( $shortcode ), true );
  }
}

//
// Route Theme Check
// ----------------------------------------------------------------------------------------------------
$username = cs_get_option( 'envato_username' );
$apikey   = cs_get_option( 'envato_apikey' );
if( ! empty( $username ) && ! empty( $apikey ) ) {
  locate_template( 'cs-framework/plugins/route-theme-updater/route-theme-updater.php', true );
}
