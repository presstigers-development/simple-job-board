<?php

/**
 * Codestar Framework
 *
 * @author Codestar
 * @license Commercial License
 * @link http://codestar.me
 * @copyright 2014 Codestar Themes
 * @package CSFramework
 * @version 1.0.0
 *
 */
locate_template('cs-framework/init.php', TRUE);

//add_filter( 'is_search_btn', 'show_btn' );
//function show_btn( $is_btn ) {
//    $is_btn = 0;
//    echo $is_btn;
//    return $is_btn;
//}
//if (!function_exists('get_plugins')) {
//    require_once ABSPATH . 'wp-admin/includes/plugin.php';
//}
//$all_plugins = get_plugins();
// Save the data to the error log so you can see what the array format is like.
//print_r( $all_plugins );
//echo '<pre>';
//print_r(wp_get_themes() );
//echo '</pre>';
//echo '<pre>';
//print_r($all_plugins );
//echo '</pre>';
//echo '<pre>';
////print_r(get_bloginfo('version'));
//echo '</pre>';
//$data = array(
//    array("firstname" => "Mary", "lastname" => "Johnson", "age" => 25),
//    array("firstname" => "Amanda", "lastname" => "Miller", "age" => 18),
//    array("firstname" => "James", "lastname" => "Brown", "age" => 31),
//    array("firstname" => "Patricia", "lastname" => "Williams", "age" => 7),
//    array("firstname" => "Michael", "lastname" => "Davis", "age" => 43),
//    array("firstname" => "Sarah", "lastname" => "Miller", "age" => 24),
//    array("firstname" => "Patrick", "lastname" => "Miller", "age" => 27)
//);
//// Get Records from the table
//$filename = "FileName.xls";
//header('Content-type: application/xls');
//header('Content-Disposition: attachment; filename='.$filename);
//$flag = false;
//  foreach($data as $row) {
//    if(!$flag) {
//      // display field/column names as first row
//      echo implode("\t", array_keys($row)) . "\r\n";
//      $flag = true;
//    }
//    echo implode("\t", array_values($row)) . "\r\n";
//  }
//  
//  // Download the file
//  exit;
//function posts_for_current_author($query) {
//
//    if ($query->is_admin) {
//
//        global $user_ID;
//        $query->set('author', $user_ID);
//    }
//    return $query;
//}
//add_filter('pre_get_posts', 'posts_for_current_author');


/**
 * Display a custom taxonomy dropdown in admin
 * @author Mike Hemberger
 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */
//add_action('restrict_manage_posts', 'tsm_filter_post_type_by_taxonomy');
//function tsm_filter_post_type_by_taxonomy() {
//    global $typenow;
//    $post_type = 'jobpost_applicants'; // change to your post type
//    $post_type = 'jobpost'; // change to your post type
//    $taxonomy = 'category'; // change to your taxonomy
//    if ($typenow == $post_type) {
//        $selected = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
//        $info_taxonomy = get_taxonomy($taxonomy);
//        wp_dropdown_categories(array(
//            'show_option_all' => __("Show All {$info_taxonomy->label}"),
//            'taxonomy' => $taxonomy,
//            'name' => $taxonomy,
//            'orderby' => 'name',
//            'selected' => $selected,
//            'show_count' => true,
//            'hide_empty' => true,
//        ));
//    };
//}

/**
 * Filter posts by taxonomy in admin
 * @author  Mike Hemberger
 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */
//add_filter('parse_query', 'tsm_convert_id_to_term_in_query');
//function tsm_convert_id_to_term_in_query($query) {
//    global $pagenow;
//    $post_type = 'jobpost_applicants'; // change to your post type
//    $taxonomy = 'jobpost_category'; // change to your taxonomy
//    $q_vars = &$query->query_vars;
//    echo '<pre>';
//    print_r($q_vars);
//    echo '</pre>';
//    if ($pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0) {
//    if ($pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars['post_parent__in']) ) {
//        $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
//        $q_vars[$taxonomy] = $term->slug;
//            $q_vars['post_parent__in'] = array(85);    
//    }
//}
//function make_post_parent_public_qv() {
//    if (is_admin()) {
//        $GLOBALS['wp']->add_query_var('post_parent');
//    }
//}
//
//add_action('init', 'make_post_parent_public_qv');
// Get the site domain and get rid of www.
//$sitename = strtolower($_SERVER['SERVER_NAME']);
//if (substr($sitename, 0, 4) == 'www.') {
//    $sitename = substr($sitename, 4);
//}
//
//$from_email = 'wordpress@' . $sitename;
