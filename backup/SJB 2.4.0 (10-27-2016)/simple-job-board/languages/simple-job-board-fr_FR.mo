��    l      |  �   �      0	     1	  "   =	  !   `	     �	  	   �	     �	  
   �	     �	     �	     �	  
   �	  	   �	  
   �	     �	     
     
     !
  
   /
     :
     C
     P
     ]
     m
     }
     �
     �
     �
      �
      �
  0      (   1  3   Z     �     �     �  %   �     �  #      '   $      L     m     �     �     �     �     �  
   �     �     �       	             #     3  	   @     J     Y     f     o     |     �     �     �     �  	   �     �     �     �     �     �     �               *  	   9  
   C     N  ,   c     �     �     �  W   �  ?   1     q     }     �     �     �  	   �  (   �     �     �  	   �            &     +   @     l  	   q     {     �     �     �  (   �  A   �       C   )     m     n     �  ,   �  
   �     �     �  
   �               5  	   A  	   K  
   U     `     v     �     �     �  
   �     �     �     �             !   %  '   G  	   o  %   y  #   �  J   �  7     G   F     �     �  "   �  3   �     	     (  !   E     g  "   �     �     �     �     �                 	   '     1     F     S     Z     j     �     �     �     �     �     �               /     @     N     ]  
   e     p  
   u     �     �  "   �     �     �  	   �  
   �       J      +   k  -   �  
   �  W   �  J   (     s          �     �  
   �     �  -   �  	   �        	        "     3  3   ;  1   o     �     �     �     �     �     �  ,   �  B        T  C   k     0   l   f       #                      V       B      T      L       
   K       W                =   8            M   	           A       a   C   3   c   R   [   ;   %   (   9   "       @   ^                        N   1   2      _   h   I   '               7          F       S      Z   \   !       U   X      i      g   d       `   P       e   Y       ,   &       j   O   $                   +      D   5      ]   :   J   -      6       >   ?   <       4   k   )      *          G           .   E                              b   Q          /       H            %s Features A valid email address is required. A valid phone number is required. Add %s Add Field Add New Add New %s Add New Feature Admin Email: All %s Appearance Applicant Applicants Application Form Fields Application Notes Apply Online Attach Resume Categories Category Company Logo Company Name Company Tagline Company Website Date Default Application Form Fields Default Feature List Delete Display job listing in grid view Display job listing in list view Display job listing with company logo and detail Display job listing without company logo Display job listing without company logo and detail Edit %s Email Notifications Enable Email Notification Enable Uploaded Files Anti-Hotlinking Enable all extensions Enable the Admin email notification Enable the Applicant email notification Enable the HR email notification Enable the Job Category Filter Enable the Job Location Filter Enable the Job Type Filter Enable the Search Bar Feature Field Field Name Filters General General Options HR Email: Job Job Applied for Job Archives Job Board Job Categories Job Category Job Data Job Features Job Listing Contents Job Listing Views Job Location Job Locations Job Type Job Types Jobs Keywords Location New %s New %s Name No %s found No %s found in trash No Categories No jobs found. Parent %s Parent %s: Please Attach Resume Please fill out application form field name. Please fill out feature name. Please fill out job feature. Posted %s ago Powerful & Robust plugin to create a Job Board on your website in simple & elegant way. Press and hold down the Ctrl key to select multiple extensions. PressTigers Required Resume Save Changes Search Search %s Select filters that display on front-end Settings Settings have been saved. Shortcode Simple Job Board Submit This is not an allowed file extension. This is where you can create and manage %s. Type Update %s Upload Upload File Extensions Value View %s Your application could not be processed. Your application has been received. We will get back to you soon. http://pressTigers.com https://wordpress.org/plugins/simple-job-board/simple-job-board-uri Project-Id-Version: Simple Job Board 2.1.0
POT-Creation-Date: 2015-10-19 16:02+0200
PO-Revision-Date: 2016-10-13 14:35+0500
Language-Team: PressTigers <support@presstigers.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.7
X-Poedit-KeywordsList: __;_e;gettext;gettext_noop
X-Poedit-Basepath: /wp-content/plugins/simple-job-board
Last-Translator: PressTigers <support@presstigers.com>
Plural-Forms: nplurals=2; plural=(n > 1);
Language: fr_FR
 %s Caractéristiques Un courriel valide est requise Un numéro de téléphone valide est requise Ajouter %s Ajouter champ Ajouter emploi Ajouter %s Ajouter caractéristique Email de l'administrateur Tout les %s Apparence Postulant Postulants Champs de postulation Notes de postulation Postuler en ligne Joindre un CV Catégories Catégorie Logo de l'entreprise Nom de l'entreprise Description de l'entreprise Site Web de l'entreprise Date Champs de pustulation par défaut Liste des caractéristiques par défaut Supprimer Emplois d'affichage dans les réseaux Emplois d'affichage dans les listes Afficher la liste des travaux avec le logo de l'entreprise et les détails Afficher la liste des emplois sans logo de l'entreprise Afficher la liste des emplois sans logo de l'entreprise et les détails Modifier %s Notifications par email Activer les emails de notification Activer l'anti HotLinking sur les fichiers uploadé Autoriser toutes les extension Activer les mails aux admins Activer les emails aux postulants Activer les mails aux RH Activer les filtres par Catégorie Activer les filtres par Lieu Activer les filtres par Type Activer la barre de recherche Caractéristique Champ Nom du champ Filtres Général Réglages Généraux Email des RH Emploi Emploi postulé Liste des offres d'emploi Emplois Catégories emplois Catégorie emploi Données de l'emploi Caractéristiques de l'emploi Contenu de la liste des emplois Voir la liste des emplois Lieu de travail Lieux de travail Type d'emploi Types d'emploi Emplois Mots-clés Lieu Nouveau %s Nouveau %s Nom Aucun %s trouvé Aucun %s trouvé dans la corbeille Aucune Catégorie aucun résultats trouvés Parent %s Parent %s: S'il vous plaît joindre CV S'il vous plaît remplir le nom du champ du formulaire de demande d'emploi S'il vous plaît remplir nom de la fonction S'il vous plaît remplir fonction de l'emploi Publié %s Powerful & Robust plugin to create a Job Board on your website in simple & elegant way. Cliquez et maintenez la touche CTRL pour sélectioner plusieurs extensions PressTigers Requis Curriculum Vitae Changements sauvegardés. Rechercher Chercher %s Sélectioner les filtres qui seront affichés Réglages Réglages sauvegardés. Shortcode Simple Job Board Envoyer Cela ne veut pas une extension de fichier autorisé C'est ici que vous pouvez créer et organiser %s. Type Mettre à jour %s Upload Extensions de fichier Valeur Voir %s Votre candidature ne pouvait être traitée. Votre candidature a été reçue. Nous vous contacterons bientôt. http://pressTigers.com https://wordpress.org/plugins/simple-job-board/simple-job-board-uri 