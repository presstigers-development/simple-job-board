<?php
/**
 * Job Listing End
 *
 * @author 	PressTigers
 * @package     Simple_Job_Board
 * @subpackage  Simple_Job_Board/templates/listing
 * @version     2.0.0
 * @since       2.1.0
 * @since       2.4.0   Revised whole HTML template
 */
?>  
    </div> <!-- end Jobs Listing: List View -->
</div>
<!-- End: Jobs Listing -->