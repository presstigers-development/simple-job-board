<?php
/**
 * Job Listing Start
 *
 * @author 	PressTigers
 * @package     Simple_Job_Board
 * @subpackage  Simple_Job_Board/templates/listing
 * @version     2.0.0
 * @since       2.1.0
 * @since       2.4.0   Revised whole HTML template
 */
?>
<div class="sjb-listing">
    <?php
        $view = get_option('job_board_listing_view');
        $class = ( 'list-view' === $view ) ? 'list-view' : 'grid-view';
    ?>
    <!-- start Jobs Listing: List View -->
    <div class="<?php echo $class; ?>">