<?php
/**
 * Custom Post Types
 *
 * @link        https://wordpress.org/plugins/simple-job-board
 *
 * @package     Simple_Job_Board
 * @subpackage  Simple_Job_Board/includes
 * @since       1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

/**
 * Define custom post types.
 *
 * Includes all files of the custom post types for job board.
 * 
 * @since      1.0.0
 * 
 * @package    Simple_Job_Board
 * @subpackage Simple_Job_Board/includes
 * @author     PressTigers <support@presstigers.com>
 */
class Simple_Job_Board_Post_Types
{
    /**
     * Initialize the class and set its properties.
     *
     * @since   1.0.0
     */
    public function __construct()
    {
        /* Jobpost Custom Post Type */
        require_once plugin_dir_path ( __FILE__ ) . 'post-types/class-simple-job-board-post-types-jobpost.php';
        
        if (class_exists ( 'Simple_Job_Board_Post_Types_Jobpost' )) {				
            new Simple_Job_Board_Post_Types_Jobpost ();
        }
        
        /* Applicants Custom Post Type */
        require_once plugin_dir_path ( __FILE__ ) . 'post-types/class-simple-job-board-post-types-applicants.php';
        
        if (class_exists ( 'Simple_Job_Board_Post_Types_Applicants' )) {				
            new Simple_Job_Board_Post_Types_Applicants ();
        }
    }
       
}

new Simple_Job_Board_Post_Types();