<?php
if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly
/**
 * Simple_Job_Board_Add_Ons Class
 * 
 * This is used to display SJB Add-ons listing.
 * 
 * @link        https://wordpress.org/plugins/simple-job-board
 * @since       2.3.2
 * 
 * @package     Simple_Job_Board
 * @subpackage  Simple_Job_Board/admin
 * @author      PressTigers <support@presstigers.com> 
 */

class Simple_Job_Board_Add_Ons {

    /**
     * Initialize the class and set its properties.
     *
     * @since   2.2.3
     */
    public function __construct() {

        // Action - Add Settings Menu
        add_action('admin_menu', array($this, 'admin_menu'), 13);
    }

    /**
     * Add Add-ons Page Under Job Board Menu.
     * 
     * @since   2.3.2
     */
    public function admin_menu() {
        add_submenu_page('edit.php?post_type=jobpost', __('Simple Job Board Add-ons', 'simple-job-board'), __('Add-ons', 'simple-job-board'), 'manage_options', 'sjb-add-ons', array($this, 'sjb_add_ons'));
    }

    /**
     * Simple Job Board Add-ons
     * 
     * @Since   2.3.2
     */
    public function sjb_add_ons() {
        $rss = new DOMDocument();
        $rss_image = new DOMDocument();
        $rss->load('http://market.presstigers.com/feed/?post_type=product');
        $add_ons = array();
        foreach ($rss->getElementsByTagName('item') as $node) {
            if ("Simple Plugin Integration" !== $node->getElementsByTagName('title')->item(0)->nodeValue &&
                    "Plugin Integration With Extended Support" !== $node->getElementsByTagName('title')->item(0)->nodeValue
            ) {

                $item = array(
                    'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
                    'link' => $node->getElementsByTagName('link')->item(0)->nodeValue
                );
                @$rss_image->loadHTML($node->getElementsByTagName('description')->item(0)->nodeValue);
                $images = $rss_image->getElementsByTagName('img');
                foreach ($images as $image) {
                    $item['image'] = $image->getAttribute('src');
                }

                $imgs = array();
                foreach ($images as $img) {
                    $imgs[] = $img;
                }
                foreach ($imgs as $img) {
                    $img->parentNode->removeChild($img);
                }
                $str = $rss_image->savehtml();
                preg_match("/<body[^>]*>(.*?)<\/body>/is", $str, $matches);
                $rss->load($str);
                $item['excerpt'] = $matches[1];

                array_push($add_ons, $item);
            }
        }
        ?>
        <h1><?php echo __('Simple Job Board Add-ons', 'simple-job-board'); ?></h1>
        
        <!--  Add-ons Details -->
        <div class="add-ons sjb-wrap">
            <div class="container-fluid">
                <div class="row">
                    <div class="products">
                        <?php foreach ($add_ons as $add_on) { ?>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 product">
                                <article>
                                    <a href="<?php echo $add_on['link']; ?>" target="_blank">
                                        <img title="<?php echo $add_on['title']; ?>" alt="<?php echo $add_on['title']; ?>" class="img-responsive" src="<?php echo $add_on['image']; ?>">                                    
                                        <h3><?php echo $add_on['title']; ?></h3>
                                    </a>
                                    <p><?php echo $add_on['excerpt']; ?></p>
                                </article>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

}

new Simple_Job_Board_Add_Ons();
