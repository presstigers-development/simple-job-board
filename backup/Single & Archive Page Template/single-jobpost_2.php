<?php
/**
 * The Template for displaying job details.
 *
 * Override this template by copying it to yourtheme/simple_job_board/single-jobpost.php
 *
 * @author      PressTigers
 * @package     simple-job-board/templates
 * @version     1.1.0
 * @since       2.2.0
 * @since       2.2.3 Added Conditional Sidebar & the_content function
 */
get_header();
global $post;
?>

<!-- Start Main Container
================================================== -->
<div class="container sjb-container" id="container">

    <!-- Start Content Area
    ================================================== -->
    <div id="content" class="sjb-content" role="main">

        <!-- Start Content Wrapper
        ================================================== -->
        <div class="sjb-wrap">            

            <!-- Start Job Title
            ================================================== -->
            <section id="sjb-page-detail">
                <h2 id="job-title"><?php echo apply_filters('sjb_single_job_detail_page_title', get_the_title()); ?></h2>                 
            </section>
            <!-- ==================================================
            End Job Title -->

            <?php while (have_posts()) : the_post(); ?>

                <!-- Start Job Details
                ================================================== -->
                <section class = "sjb-wrap single-job-listing">                   
                    <?php
                    /** 
                     * Template -> Company Meta
                     * - Logo
                     * - Job Tilte & Company Name
                     * - Job Type
                     * - Job Location
                     * - Job Post Date
                     */
                    get_simple_job_board_template('single-jobpost/content-single-job-listing-meta.php');
                    ?>                 
                    <article class="sjb-row sjb-job-details" id="sjb_job-detail-heading">
                        <?php
                         /* Display the post content.
                          * 
                          * The "the_content" is used to filter the content of the job post. Also make other plugins shortcode compatible with job post editor. 
                          */
                         the_content(); 
                         
                        /** 
                         * Template -> Job Features
                         * - First Feature as Category Name
                         * - Remaining List of Features
                         */
                        get_simple_job_board_template('single-jobpost/job-features.php');

                        /** 
                         * Template -> Job Application Form
                         * 
                         * Display the user defined application form. 
                         */
                        get_simple_job_board_template('single-jobpost/job-application.php');
                        ?>
                    </article> 
                </section>
                <!-- ==================================================
                End Job Details -->

            <?php endwhile; ?>
        </div>
        <!-- ==================================================
        End Content Wrapper -->

        <?php
        /**
         * Load sidebar template.
         * 
         * Includes the theme sidebar template. 
         */
        get_sidebar();
        ?>
    </div>
    <!-- ==================================================
    End Content Area -->
    
</div>
<!-- ==================================================
End Main Container -->

<?php get_footer();