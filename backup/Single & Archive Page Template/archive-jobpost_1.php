<?php
/**
 * The Template for displaying job archives, including the main jobpost page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/simple_job_board/archive-jobpost.php
 *
 * @author      PressTigers
 * @package     simple-job-board/templates
 * @version     1.1.0
 * @since       2.2.0
 * @since       2.2.3 Added Conditional Sidebar
 */
get_header();
global $wp_query, $wp_rewrite, $post;
?>

<!-- Start Main Container
================================================== -->
<div class="container sjb-container" id="container">

    <!-- Start Content Area
    ================================================== -->
    <div id="content" class="sjb-content" role="main">

        <!-- Start Content Wrapper
        ================================================== -->
        <div class="sjb-wrap">            

            <!-- Start Job Title
            ================================================== -->
            <section id="sjb-page-detail">
                <h2 id="job-title"><?php echo apply_filters('sjb_jobs_archive_title', __('Job Archives', 'simple-job-board')); ?></h2>                 
            </section>
            <!-- ==================================================
            End Job Title -->

            <?php
            if (get_query_var('paged')) {
                $paged = get_query_var('paged');
            } elseif (get_query_var('page')) {
                $paged = get_query_var('page');
            } else {
                $paged = 1;
            }

            // WP_Query Arguments
            $args = apply_filters(
                    'sjb_output_jobs_args',
                    array(
                        'posts_per_page' => '15',
                        'post_type'      => 'jobpost',
                        'paged'          => $paged,
                    )
            );

            // Merge WP_Query $args array on each $_GET element            
            $args['jobpost_category'] = (isset($_GET['selected_category']) && -1 != $_GET['selected_category']) ? sanitize_text_field($_GET['selected_category']) : '';
            $args['jobpost_job_type'] = (isset($_GET['selected_jobtype']) && -1 != $_GET['selected_jobtype']) ? sanitize_text_field($_GET['selected_jobtype']) : '';
            $args['jobpost_location'] = (isset($_GET['selected_location']) && -1 != $_GET['selected_location']) ? sanitize_text_field($_GET['selected_location']) : '';
            $args['s'] = (isset($_GET['selected_location'])) ? sanitize_text_field($_GET['search_keywords']) : '';

            $wp_query = new WP_Query($args);
            ?>

            <?php
            /**
             * Fires before filters on job listing page
             * 
             * @since 2.1.0
             */
            do_action('simple_job_board_job_filters_before');
            ?>

            <!-- Start Job Filters
            ================================================== -->
            <article id="sjb-contain-bg" <?php post_class('sjb-job-filters'); ?>>
                <div class="sjb-row">
                    <div class="sjb-form-inline">                       
                        <?php
                        /**
                         * Job Filters Form 
                         * 
                         * - Keywords Search
                         * - Job Category Filter
                         * - Job Type Filter
                         * - Job Location Filter
                         * 
                         * Search jobs by category, job location, job type and keywords.
                         */
                        ?>
                        <form class="sjb-job-filters-form" action="<?php echo apply_filters('sjb_job_filters_form_action', ''); ?>" method="<?php echo apply_filters('sjb_job_filters_form_method', 'get'); ?>">
                            <?php
                            /**
                             * Fires before keyword search on job listing page.
                             * 
                             * @since 2.1.0
                             */
                            do_action('simple_job_board_job_filters_start');

                            if ('yes' === get_option('job_board_search_bar')) {
                                ?>

                                <!-- Keywords Search-->
                                <div class="sjb-search-keywords sjb-col-md-12" id="sjb-form-padding">
                                    <?php
                                    $search_keyword = isset($_GET['search_keywords']) ? $_GET['search_keywords'] : '';

                                    // Append Query string With Page ID When Permalinks are not Set
                                    if (!get_option('permalink_structure')) {
                                        ?>
                                        <input type="hidden" value="<?php echo get_the_ID(); ?>" name="page_id" >
                                    <?php } ?>
                                    <input type="text" class="sjb-search-keyword sjb-form-control" value="<?php echo $search_keyword; ?>" placeholder="<?php _e('Keywords', 'simple-job-board'); ?>" id="search_keywords" name="search_keywords">
                                </div>
                                <?php
                            }

                            /**
                             * Fires before category dropdown on job listing page
                             * 
                             * @since 2.2.0
                             */
                            do_action('simple_job_board_job_filters_dropdowns_start');

                            // check for setting page option and the term existance
                            if ((NULL != get_terms('jobpost_category')) && ('yes' === get_option('job_board_category_filter'))) {

                                $selected_category = isset($_GET['selected_category']) ? $_GET['selected_category'] : FALSE;

                                /**
                                 * Creating list on non-empty job category
                                 * 
                                 * Job Category Selectbox
                                 */
                                
                                // Job Category Arguments
                                $category_args = array(
                                    'show_option_none' => __('Category', 'simple-job-board'),
                                    'orderby'          => 'NAME',
                                    'order'            => 'ASC',
                                    'hide_empty'       => 0,
                                    'echo'             => FALSE,
                                    'hierarchical'     => TRUE,
                                    'name'             => 'selected_category',
                                    'class'            => 'sjb-form-control',
                                    'selected'         => $selected_category,
                                    'taxonomy'         => 'jobpost_category',
                                    'value_field'      => 'slug',
                                );

                                // Display or retrieve the HTML dropdown list of job category
                                $category_select = wp_dropdown_categories(apply_filters('sjb_category_filter_args', $category_args));
                                ?>

                                <!-- Category Filter-->
                                <div class="sjb-search-categories <?php echo apply_filters('sjb_category_filter_class', 'sjb-col-md-4'); ?>" id="sjb-form-padding">
                                    <?php
                                    if (isset($category_select) && (NULL != $category_select )) {
                                        echo $category_select;
                                    }
                                    ?>
                                </div>
                                <?php
                            }
                            
                            // Check For Settings Option and the Term Existance
                            if (NULL != get_terms('jobpost_job_type') && 'yes' === get_option('job_board_jobtype_filter')) {

                                $selected_jobtype = isset($_GET['selected_jobtype']) ? $_GET['selected_jobtype'] : FALSE;

                                /**
                                 * Creating list on non-empty job type
                                 * 
                                 * Job Type Selectbox
                                 */
                                
                                // Job Type Arguments
                                $jobtype_args = array(
                                    'show_option_none' => __('Job Type', 'simple-job-board'),
                                    'orderby'          => 'NAME',
                                    'order'            => 'ASC',
                                    'hide_empty'       => 0,
                                    'echo'             => FALSE,
                                    'name'             => 'selected_jobtype',
                                    'class'            => 'sjb-form-control',
                                    'selected'         => $selected_jobtype,
                                    'hierarchical'     => TRUE,
                                    'taxonomy'         => 'jobpost_job_type',
                                    'value_field'      => 'slug',
                                );

                                // Display or retrieve the HTML dropdown list of job type
                                $jobtype_select = wp_dropdown_categories(apply_filters('sjb_job_type_filter_args', $jobtype_args));
                                ?>   

                                <!-- Job Type Filter-->
                                <div class="sjb-search-job-type <?php echo apply_filters('sjb_job_type_filter_class', 'sjb-col-md-3'); ?>" id="sjb-form-padding">
                                    <?php
                                    if (NULL != $jobtype_select) {
                                        echo $jobtype_select;
                                    }
                                    ?>
                                </div>
                                <?php
                            }

                            // Check For Settings Option and the Term Existance
                            if (NULL != get_terms('jobpost_location') && 'yes' === get_option('job_board_location_filter')) {
                                $selected_location = isset($_GET['selected_location']) ? $_GET['selected_location'] : FALSE;

                                /**
                                 * Creating list on non-empty job location
                                 * 
                                 * Job Location Selectbox
                                 */

                                // Job Location Arguments
                                $jobloc_args = array(
                                    'show_option_none' => __('Location', 'simple-job-board'),
                                    'orderby'          => 'NAME',
                                    'order'            => 'ASC',
                                    'hide_empty'       => 0,
                                    'echo'             => FALSE,
                                    'name'             => 'selected_location',
                                    'class'            => 'sjb-form-control',
                                    'selected'         => $selected_location,
                                    'hierarchical'     => TRUE,
                                    'taxonomy'         => 'jobpost_location',
                                    'value_field'      => 'slug',
                                );

                                // Display or retrieve the HTML dropdown list of job locations
                                $jobloc_select = wp_dropdown_categories(apply_filters('sjb_job_location_filter_args', $jobloc_args));
                                ?>    

                                <!-- Job Location Filter-->
                                <div class="sjb-search-location <?php echo apply_filters('sjb_job_location_filter_class', 'sjb-col-md-3'); ?>" id="sjb-form-padding">
                                    <?php
                                    if (NULL != $jobloc_select)
                                        echo $jobloc_select;
                                    ?>
                                </div>
                                <?php
                            }

                            /**
                             * Fires after job location dropdown on job listing page.
                             * 
                             * @since 2.2.0
                             */
                            do_action('simple_job_board_job_filters_dropdowns_end');

                            if ((isset($category_select) && NULL != $category_select) || ( isset($jobtype_select) && NULL != $jobtype_select) || ( isset($jobloc_select) && NULL != $jobloc_select) || 'yes' === get_option('job_board_search_bar')) {
                                $search_button = '<div class="sjb-search-button ' . apply_filters('sjb_filters_button_class', 'sjb-col-md-2') . '" id="sjb-form-padding">'
                                        . '<input type="submit" class="sjb-search" value=""/>'
                                        . '</div>';
                                echo apply_filters('sjb_job_filters_search_button', $search_button);
                            }
                            ?>
                        </form>
                    </div>
                </div>
            </article>
            <!-- ==================================================
            End Job Filters -->

            <?php
            /**
             * Fires after job filters on job listing page
             * 
             * @since 2.1.0
             */
            do_action('simple_job_board_job_filters_after');

            if ($wp_query->have_posts()) :

                /**
                 * Fires at start of job listings on job listing page.
                 * 
                 * @since 2.2.0
                 */
                do_action('sjb_job_listing_before');

                global $counter;
                $counter = 1;
                while ($wp_query->have_posts()) : $wp_query->the_post();
                    if ('grid-view' === get_option('job_board_listing_view')) {
                        global $counter;

                        if (1 === $counter % 3) {
                            echo '<section class="sjb-row">';
                        }
                        ?>

                        <!-- Start Jobs Grid View 
                        ================================================== -->
                        <article class="sjb-col-md-4 <?php post_class('sjb-job-grid-view'); ?>" >
                            <div id="sjb_job-visiable">
                                <div class="sjb-row">
                                    
                                    <!-- Grid view header -->
                                    <header class="sjb-col-md-12">
                                        <div class="sjb-row">

                                            <!-- Company Logo -->
                                            <div id="sjb_company-logo" class="company-logo">
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php sjb_the_company_logo(); ?>
                                                </a>
                                            </div>

                                            <!-- Job Title & Company Name -->
                                            <div id="sjb-grid-view-heading">
                                                <h4 id="sjb_job-heading" class="company-name">
                                                    <a href="<?php the_permalink(); ?>">
                                                        <?php
                                                        the_title();
                                                        if (sjb_get_the_company_name()) {
                                                            ?>
                                                            | <?php
                                                            sjb_the_company_name();
                                                        }
                                                        ?> 
                                                    </a>    
                                                </h4>
                                            </div>
                                        </div>
                                    </header>

                                    <!-- Job Type -->
                                    <div class="sjb-col-md-12">
                                        <?php if ($job_type = sjb_get_the_job_type()) {
                                            ?>
                                            <div id="sjb_job-bolits">
                                                <i class="fa fa-clock-o"></i><?php sjb_the_job_type(); ?>
                                            </div>
                                        <?php } ?>
                                    </div>

                                    <!-- Job Location -->
                                    <div class="sjb-col-md-12">
                                        <?php if ($job_location = sjb_get_the_job_location()) {
                                            ?>
                                            <div id="sjb_job-bolits">
                                                <i class="fa fa-location-arrow"></i><?php sjb_the_job_location(); ?>
                                            </div>
                                        <?php } ?>
                                    </div>

                                    <!-- Job Posting Time -->
                                    <div class="sjb-col-md-12">
                                        <?php if ($job_posting_time = sjb_get_the_job_posting_time()) {
                                            ?>
                                            <div id="sjb_job-bolits">
                                                <i class="fa fa-calendar"></i><?php sjb_the_job_posting_time(); ?>
                                            </div>
                                        <?php } ?>
                                    </div>

                                    <!-- Job Description -->
                                    <div class="sjb-col-md-12">
                                        <div class="sjb-row">
                                            <div class="sjb-lead job-description">
                                                <?php the_excerpt(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <!-- ==================================================
                        End Jobs Grid View -->

                        <?php
                        if (0 === $counter % 3)
                            echo '</section>';
                        $counter++;
                        } else {
                        ?>

                        <!-- Start Jobs List View 
                        ================================================== -->
                        <article id="sjb_job-visiable" class="sjb-list-view">
                            <div class="sjb-row">

                                <!-- Job Listing Header -->
                                <header class="sjb-col-md-6">
                                    <div class="sjb-row">

                                        <!-- Company Logo -->
                                        <div id="sjb_company-logo-full-view" class="company-logo">
                                            <a href="<?php the_permalink(); ?>"><?php sjb_the_company_logo(); ?></a>
                                        </div>

                                        <!-- Job Title & Company Name -->
                                        <div id="sjb-heading">
                                            <h4 id="sjb_job-heading" class="company-name">
                                                <a href="<?php the_permalink(); ?>"><?php
                                                    the_title();

                                                    if (sjb_get_the_company_name()) {
                                                        ?>
                                                        | <?php
                                                        sjb_the_company_name();
                                                    }
                                                    ?> 
                                                </a>
                                            </h4>
                                        </div>     
                                    </div>
                                </header>

                                <!-- Job Type -->
                                <div class="sjb-col-md-2">
                                    <?php if ($job_type = sjb_get_the_job_type()) {
                                        ?>
                                        <div id="sjb_job-bolits">
                                            <i class="fa fa-clock-o"></i><?php sjb_the_job_type(); ?>
                                        </div>
                                    <?php } ?> 
                                </div>

                                <!-- Job Location -->
                                <div class="sjb-col-md-2">
                                    <?php if ($job_location = sjb_get_the_job_location()) {
                                        ?>
                                        <div id="sjb_job-bolits">
                                            <i class="fa fa-location-arrow"></i><?php sjb_the_job_location(); ?>
                                        </div>
                                    <?php } ?>
                                </div>

                                <!-- Job Post Date -->
                                <div class="sjb-col-md-2">
                                    <?php if ($job_posting_time = sjb_get_the_job_posting_time()) {
                                        ?>
                                        <div id="sjb_job-bolits">
                                            <i class="fa fa-calendar"></i><?php sjb_the_job_posting_time(); ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>

                            <!-- Job Description -->
                            <div class="sjb-row">
                                <div class="sjb-lead job-description">
                                    <?php the_excerpt(); ?>
                                </div>
                            </div>
                        </article>
                        <!-- ==================================================
                        End Jobs List View -->

                        <?php
                    }
                endwhile; // end of the loop.

                /**
                 * Fires at the end of job listings on job listing page.
                 * 
                 * @since 2.2.0
                 */
                do_action('sjb_job_listing_after');
            else:
                // archive-jobpost no-job listing               
                echo '<div class="no-job-listing">' . __('No jobs found.', 'simple-job-board') . '</div>';
            endif;

            /**
             * Job listing pagination
             * 
             * Show pagiantion after displaying fifteen jobs
             */
            $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;

            // Pagination Arguments
            $pagination_args = array(
                'base'      => @add_query_arg('page', '%#%'),
                'format'    => '',
                'total'     => $wp_query->max_num_pages,
                'current'   => $current,
                'show_all'  => TRUE,
                'next_text' => 'Next',
                'prev_text' => 'Previous',
                'type'      => 'list',
            );
            $pagination = apply_filters('sjb_pagination_links_default_args', $pagination_args);

            /**
             * Modify query string
             *  
             * Remove query "page" argument on job search  
             */
            if (!(isset($_GET['selected_category']) || isset($_GET['selected_jobtype']) || isset($_GET['selected_location']) || isset($_GET['search_keywords']))) {
                if ($wp_rewrite->using_permalinks())
                    $pagination['base'] = user_trailingslashit(trailingslashit(remove_query_arg('page', get_pagenum_link(1))) . '?page=%#%/', 'paged');

                if (!empty($wp_query->query_vars['s']))
                    $pagination['add_args'] = array('s' => get_query_var('s'));
            }

           // Retrieve paginated link for archive jobpost page
            echo paginate_links($pagination);
            
            wp_reset_query();
            ?>
        </div>
        <!-- ==================================================
        End Content Wrapper -->
        <?php
        /**
         * Load sidebar template.
         * 
         * Includes the theme sidebar template. 
         */
        get_sidebar();
        ?>
    </div>
    <!-- ==================================================
    End Content Area -->
    
</div>
<!-- ==================================================
End Main Container -->

<?php get_footer();