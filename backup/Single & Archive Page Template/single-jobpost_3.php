<?php
global $post;
/**
 * The Template for displaying job details.
 *
 * Override this template by copying it to yourtheme/simple_job_board/single-jobpost.php
 *
 * @author      PressTigers
 * @package     simple-job-board/templates
 * @version     1.1.0
 * @since       2.2.0
 * @since       2.2.3 Added Conditional Sidebar & the_content function
 */
get_header();

/**
 * Hook -> sjb_before_main_content
 * 
 * - Output Opening div of Main Container.
 * - Output Opening div of Content Area.
 *
 * @hooked sjb_job_listing_wrapper_start - 10 
 * @since  2.2.0
 * @since  2.2.3 Removed the content wrapper opening div
 */
do_action('sjb_before_main_content');
?>

<!-- Start Content Wrapper
================================================== -->
<div class="sjb-wrap">            

    <!-- Start Job Title
    ================================================== -->
    <section id="sjb-page-detail">
        <h2 id="job-title"><?php echo apply_filters('sjb_single_job_detail_page_title', get_the_title()); ?></h2>                 
    </section>
    <!-- ==================================================
    End Job Title -->

    <?php while (have_posts()) : the_post(); ?>

        <!-- Start Job Details
        ================================================== -->
        <section class = "sjb-wrap single-job-listing">                   
            <?php
            /**
             * Template -> Company Meta
             * - Logo
             * - Job Tilte & Company Name
             * - Job Type
             * - Job Location
             * - Job Post Date
             */
            get_simple_job_board_template('single-jobpost/content-single-job-listing-meta.php');
            ?>                 
            <article class="sjb-row sjb-job-details" id="sjb_job-detail-heading">
                <?php
                /* Display the post content.
                 * 
                 * The "the_content" is used to filter the content of the job post. Also make other plugins shortcode compatible with job post editor. 
                 */
                the_content();

                /**
                 * Template -> Job Features
                 * - First Feature as Category Name
                 * - Remaining List of Features
                 */
                get_simple_job_board_template('single-jobpost/job-features.php');

                /**
                 * Template -> Job Application Form
                 * 
                 * Display the user defined application form. 
                 */
                get_simple_job_board_template('single-jobpost/job-application.php');
                ?>
            </article> 
        </section>
        <!-- ==================================================
        End Job Details -->

    <?php endwhile; ?>
</div>
<!-- ==================================================
End Content Wrapper -->

<?php
/**
 * Load sidebar template.
 * 
 * Includes the default theme sidebar template. 
 */
get_sidebar();

/**
 * Hook -> sjb_after_main_content
 * 
 * - Output Opening div of Main Container.
 * - Output Opening div of Content Area.
 * 
 * @hooked sjb_job_listing_wrapper_end - 10 (outputs closing divs for the content)
 * @since  2.2.0
 * @since  2.2.3 Removed the content wrapper closing div
 */
do_action('sjb_after_main_content');

get_footer();